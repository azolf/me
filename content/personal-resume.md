---
title: "Personal Resume"
date: 2023-06-15T17:00:00+04:30
draft: false
author: Amirhosein Zolfaghari
description: Personal Resume
---

Experienced Engineering Lead with a demonstrated track record of leading cross-functional teams to deliver high-quality products and solutions. With over ten years in the industry, I bring a unique blend of technical expertise and strong leadership skills to drive growth and innovation.
Adept at leveraging agile methodologies and collaboration to drive results, I am passionate about creating a culture of excellence and fostering a positive work environment.

Specialties include mainly Ruby, PHP, PostgreSQL, and Ansible.

Independence and transparency hold significant value for me. I prefer being direct and concise. Efficiency is crucial, and I possess a high tolerance for risk. I tend to approach things from an abstract perspective, without getting too caught up in the details.

Disorderliness and lack of planning are sources of annoyance for me. I have little patience for ambiguity, as it negatively affects my state of mind. I find no tolerance for constant complaining.

Text-based communication is the most effective way to reach me, as phone calls tend to stress me out.

When I speak earnestly, my voice naturally becomes louder, often leading others to mistake my passion for anger. However, it is not necessarily the case. When I am genuinely angry, there might be a slight change in my accent.


Seeking new opportunities to continue making an impact in the technology sector.
